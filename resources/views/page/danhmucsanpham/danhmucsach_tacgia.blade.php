@extends('page.danhmucsanpham.danhmucsach_mater')
@section('content_danhmuc')
    @foreach ($tacgia as $item)
            <div class="product">
                <div class="image">
                    <div style="position: relative;">
                        <a href="{{ route('chitietsanpham',$item->id) }}"><img src="public/image/anhsanpham/{{ $item->anhbia }}" alt="" class=""></a>
                        <span class="saleprice" style="background:url({{ url('public/image/saleprice.png') }}) no-repeat;"></span>
                    </div>
                    <a href="{{ route('chitietsanpham',$item->id) }}">
                        <div class="product_name" title="{{ $item->tensach }}">{{ $item->tensach }}</div>
                    </a>
                    <div class="product_composer">Tác giả: {{ $item->tacgia }}</div>
                    <div class="prices">{{ number_format($item->dongia - $item->dongia * $item->khuyenmai/100) }}₫</div>
                    <div class="rootprices">{{ number_format($item->dongia) }}₫</div>
                    <div class="rating">
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star checked"></span>
                        <span class="fa fa-star"></span>
                        <span class="fa fa-star"></span>
                        <span class="comment">(30 nhận xét)</span></div>
                </div>
            </div>
    @endforeach
@endsection