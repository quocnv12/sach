<div class="sortable" id="layoutGroup3">
    <div class="block" id="module_categories">
        <h2>Danh mục</h2>
        <div class="blockcontent">
            <ul>
                @foreach($ls as $item)
                    @if($item->madanhmuc == $id)
                        <li><a href="{{ route('loai.sach',$item->tenloai) }}" title="{{ $item->tenloai }}">{{ $item->tenloai }}</a></li>
                    @endif
                @endforeach
            </ul>
            <div class="clear"></div>
        </div>
    </div>
    <div class="block" id="module_listfields">
        <div class="fields_contener">
            <h2>Tác giả</h2>
            <div class="blockcontent">
                <div class="showboxfield">
                    <ul>
                        @foreach ($loaisach as $items)
                            @foreach ($items->sachs as $item)
                                @php
                                    $tacgia = \App\sach::where('tacgia', $item->tacgia)->get()
                                @endphp
                                <a href="{{ route('danhmuc/tacgia',[$items->id ,$item->tacgia]) }}" title="" class="checkboxs">
                                <li  class="checkbox">
                                <input type="radio" name="checktacgia"> {{ $item->tacgia }}<span class="count">({{ count($tacgia) }})</span>
                                </li>
                                </a>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
            
        </div>
        <div class="fields_contener">
            <h2>Thương hiệu</h2>
            <div class="blockcontent">
                <div class="showboxfield">
                    <ul>
                        @foreach ($loaisach as $items)
                            @foreach ($items->sachs as $item)
                            @php
                                $count_nxban = \App\sach::where('manhaxuatban', $item->nhaxuatban->id)->get();
                            @endphp
                                <a href="{{ route('danhmuc/nxban',[$items->id, $item->nhaxuatban->tennhaxuatban]) }}" title="Thái Hà">
                                    <li  class="checkbox">
                                        <input type="radio" name="checknxb"> {{ $item->nhaxuatban->tennhaxuatban }}</i> <span class="count">({{ count($count_nxban) }})</span>
                                        <span class="delete"></span>
                                    </li>
                                    </a>
                            @endforeach
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>
</div>